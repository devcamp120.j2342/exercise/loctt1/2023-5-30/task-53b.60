public class SuperAnimalClass{
    public static void main(String[] args) throws Exception {
       Animal animal1 = new Animal("Vit");

       System.out.println("Animal 1:");
       System.out.println(animal1);
        System.out.println("--------------------------------------");

        Animal animal2 = new Animal("Ga");

        System.out.println("Animal 2:");
        System.out.println(animal2);
         System.out.println("--------------------------------------");

         Mammal mammal1 = new Mammal("Bo");
         System.out.println("Mammal 1:");
         System.out.println(mammal1);
         System.out.println("--------------------------------------");

         
         Mammal mammal2 = new Mammal("Cuu");
         System.out.println("Mammal 2:");
         System.out.println(mammal2);
         System.out.println("--------------------------------------");

         Cat cat1 = new Cat("Myo");
         System.out.println("Cat 1:");
         System.out.println(cat1);
         System.out.println("--------------------------------------");

         Cat cat2 = new Cat("Min");
         System.out.println("Cat 2:");
         System.out.println(cat2);
         System.out.println("--------------------------------------");

         Dog dog1 = new Dog("Muc");
         System.out.println("Dog 1:");
         System.out.println(dog1);
         System.out.println("--------------------------------------");

         Dog dog2 = new Dog("Den");
         System.out.println("Dog 2:");
         System.out.println(dog2);
         System.out.println("--------------------------------------");
    }
}

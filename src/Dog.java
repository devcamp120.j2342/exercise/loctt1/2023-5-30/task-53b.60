public class Dog extends Mammal{

     public Dog(String name) {
          super(name);
          //TODO Auto-generated constructor stub
     }

     public void greets(){
          System.out.println("Woof");
     }

     public void greets(Dog another){
          System.out.println("Woooooooof");

     }

     @Override
     public String toString(){
          return String.format("Dog[Mammal[Animal [Name = %s]]]", name);
     }
}
